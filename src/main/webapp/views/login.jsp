<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title>Login</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
	<script src="https://code.jquery.com/jquery-2.2.4.js" integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI=" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
	<style type="text/css">
		.content{
			border: 1px solid #ddd;
			padding: 10px;
			box-shadow: 5px 5px 20px 2px grey;
			margin-top: 20%;
		}

		body{
			background: #fdfdfd;
		}
	</style>
</head>
<body>
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-4 col-md-4 col-sm-3"></div>
			<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
				<div class="content" id="hello">
					<form action="<%=request.getContextPath()%>/login-action" method="post" role="form">
						<center><legend style="padding: 10px; font-size: 3rem; font-weight: bold">Login</legend></center>
						<div class="form-group">
							<label for="username">Username</label>
							<input type="text" class="form-control" id="username" name="tenDangNhap" placeholder="Username">
							<s:fielderror fieldName="username"></s:fielderror>
						</div>
						<div class="form-group">
							<label for="password">Password</label>
							<input type="password" class="form-control" id="password" name="matKhau" placeholder="Password">
						</div>
						<div>
							<div class="checkbox">
								<label>
									<input type="checkbox" value="">Save
								</label>
							</div>
						</div>
						<label><s:actionmessage/></label>
						<center><button type="submit" id="btnSubmit" class="btn btn-default">Submit</button></center>
					</form>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-3">
			</div>
		</div>
	</div>
	<script type="text/javascript">
	</script>
</body>
</html>