<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title>Người sử dụng</title>
	<!-- Latest compiled and minified CSS & JS -->
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
	<script src="https://code.jquery.com/jquery-2.2.4.js" integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI=" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
	<style>
		.close:hover{
			color:red;
			opacity: 1;
		}
	</style>
</head>
<body>
	<nav class="navbar navbar-default" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#">TTHC</a>
			</div>
	
			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse navbar-ex1-collapse">
				<ul class="nav navbar-nav">
					<li><a href="#">Người sử dụng</a></li>
					<li><a href="#">Quản lý nhóm</a></li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">User <b class="caret"></b></a>
						<ul class="dropdown-menu">
							<li><a href="#">Action</a></li>
							<li><a href="#">Another action</a></li>
							<li><a href="#">Something else here</a></li>
							<li><a href="#">Separated link</a></li>
						</ul>
					</li>
				</ul>
			</div><!-- /.navbar-collapse -->
		</div>
	</nav>
	<div class="container">
		<div role="tabpanel">
			<!-- Nav tabs -->
			<ul class="nav nav-tabs" role="tablist">
				<li role="presentation">
					<a href="#home" aria-controls="new" class="btn choose" role="tab" data-toggle="tab"  data-target="#new">New</a>
				</li>
				<li role="presentation">
					<a href="#tab" aria-controls="search" class="btn choose" role="tab" data-toggle="tab" data-target="#search">Search</a>
				</li>
			</ul>
			<!-- Tab panes -->
			<div class="tab-content">
				<div role="tabpanel" class="tab-pane" id="new">
					<div class="container-fluid">
						<div class="row"><span class="close btn btn-sm" onclick="closeModal()">&times;</span></div>
						<div class="row">
							<form>
								<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
									<div class="form-group">
										<label for="coquanMa">Mã</label>
										<input type="text" class="form-control" id="coquanMa" placeholder="Mã cơ quan">
									</div>
								</div>
								<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
									<div class="form-group">
										<label for="dnntienDiachi">Địa chỉ</label>
										<input type="text" class="form-control" id="dnntienDiachi" placeholder="Địa chỉ">
									</div>
								</div>
								<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
									<div class="form-group">
										<label for="ngayNoptien">Ngày nộp</label>
										<input type="date" class="form-control" id="ngayNoptien">
									</div>
								</div>
								<div class="col-xs-12">
									<button type="submit" class="btn btn-sm btn-primary pull-right">Submit</button>
								</div>
							</form>
						</div>
					</div>
				</div>
				<div role="tabpanel" class="tab-pane border-secondary" id="search">
					<div class="container-fluid">
						<div class="row"><span class="close btn btn-sm" onclick="closeModal()">&times;</span></div>
						<div class="row">
							<form>
								<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
									<div class="form-group">
										<label for="coquanMa">Mã</label>
										<input type="text" class="form-control" id="coquanMa" placeholder="Mã cơ quan">
									</div>
								</div>
								<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
									<div class="form-group">
										<label for="dnntienDiachi">Địa chỉ</label>
										<input type="text" class="form-control" id="dnntienDiachi" placeholder="Địa chỉ">
									</div>
								</div>
								<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
									<div class="form-group">
										<label for="ngayNoptien">Ngày nộp</label>
										<input type="date" class="form-control" id="ngayNoptien">
									</div>
								</div>
								<div class="col-xs-12">
									<button type="submit" class="btn btn-sm btn-primary pull-right">Search</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<script>
			function closeModal(){
				$(".tab-pane").removeClass('active');
				$(".choose").attr('aria-expanded','false');
				$(".choose").parent().removeClass('active');
			}
		</script>
	</div>

	<div class="container">
		<div class="table-responsive">
			<table class="table table-hover">
				<thead>
					<tr>
						<th>STT</th>
						<th>Ma co quan</th>
						<th>Dia chi</th>
						<th>Ngay nop</th>
						<th colspan="3" width="5%"></th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>1</td>
						<td>Coquan1</td>
						<td>Ha noi</td>
						<td>2020-02-02</td>
						<td><button class="btn btn-sm btn-default">Detail</button></td>
						<td><button class="btn btn-sm btn-primary">Update</button></td>
						<td><button class="btn btn-sm btn-warning">Delete</button></td>
					</tr>
					<tr>
						<td>1</td>
						<td>Coquan1</td>
						<td>Ha noi</td>
						<td>2020-02-02</td>
						<td><button class="btn btn-sm btn-default">Detail</button></td>
						<td><button class="btn btn-sm btn-primary">Update</button></td>
						<td><button class="btn btn-sm btn-warning">Delete</button></td>
					</tr>
					<tr>
						<td>1</td>
						<td>Coquan1</td>
						<td>Ha noi</td>
						<td>2020-02-02</td>
						<td><button class="btn btn-sm btn-default">Detail</button></td>
						<td><button class="btn btn-sm btn-primary">Update</button></td>
						<td><button class="btn btn-sm btn-warning">Delete</button></td>
					</tr>
					<tr>
						<td>1</td>
						<td>Coquan1</td>
						<td>Ha noi</td>
						<td>2020-02-02</td>
						<td><button class="btn btn-sm btn-default">Detail</button></td>
						<td><button class="btn btn-sm btn-primary">Update</button></td>
						<td><button class="btn btn-sm btn-warning">Delete</button></td>
					</tr>
				</tbody>
			</table>
			Records: <select class="btn btn-sm btn-default">
						<option value="5">5</option>
						<option value="10">10</option>
						<option value="20">20</option>
						<option value="30">30</option>
					</select><br>
			<ul class="pagination">
				<li><a href="#">&laquo;</a></li>
				<li><a href="#">1</a></li>
				<li><a href="#">2</a></li>
				<li><a href="#">3</a></li>
				<li><a href="#">4</a></li>
				<li><a href="#">5</a></li>
				<li><a href="#">&raquo;</a></li>
			</ul>
		</div>
	</div>
	<a class="btn btn-primary" data-toggle="modal" href='#modal-id'>Trigger modal</a>
	<div class="modal fade" id="modal-id">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Th</h4>
				</div>
				<div class="modal-body">
					
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary">Save changes</button>
				</div>
			</div>
		</div>
	</div>
</body>
</html>