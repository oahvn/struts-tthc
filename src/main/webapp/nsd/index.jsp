<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title>Người sử dụng</title>
	<!-- Latest compiled and minified CSS & JS -->
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
	<script src="https://code.jquery.com/jquery-2.2.4.js" integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI=" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
	<style>
		.close:hover{
			color:red;
			opacity: 1;
		}
	</style>
</head>
<jsp:include page="../pages/autoLogout.jsp"></jsp:include>
<body>
	<nav class="navbar navbar-default" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#">TTHC</a>
			</div>
	
			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse navbar-ex1-collapse">
				<ul class="nav navbar-nav">
					<li><a href="">Người sử dụng</a></li>
					
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">${User.tenDangNhap } <b class="caret"></b></a>
						<ul class="dropdown-menu">
							<li><s:a action="logout">Logout</s:a></li>
						</ul>
					</li>
				</ul>
			</div><!-- /.navbar-collapse -->
		</div>
	</nav>
	
	<!-- FORM -->
	<div class="container">
		<%if(session.getAttribute("message")!=null){ %>
		<div class="row col-xs-12"><span class="label label-success pull-right">${message }</span></div>
		<%}
			session.removeAttribute("message");
		%>
		<div role="tabpanel">
			<!-- Nav tabs -->
			<ul class="nav nav-tabs" role="tablist">
				<li role="presentation">
					<a href="#home" aria-controls="new" class="btn choose" role="tab" data-toggle="tab"  data-target="#new">New</a>
				</li>
				<li role="presentation">
					<a href="#tab" aria-controls="search" class="btn choose" role="tab" data-toggle="tab" data-target="#search">Search</a>
				</li>
			</ul>
			${hello}
			<!-- Tab panes -->
			<div class="tab-content">
				<div role="tabpanel" class="tab-pane ${hasError==true?'active':''}" id="new">
					<div class="container-fluid">
						<div class="row"><span class="close btn btn-sm" onclick="closeModal()">&times;</span></div>
						<div class="row">
							<form action="<%=request.getContextPath()+"/nsd/save" %>" method="post" autocomplete="off">
								<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
									<div class="form-group">
										<label for="tenDangNhap">Tên đăng nhập</label>
										<input type="text" class="form-control" id="tenDangNhap" autocomplete="off" name="tenDangNhap" placeholder="Tên đăng nhập">
										<s:fielderror class="text-danger" fieldName="username"></s:fielderror>
										<div id="message" class="text-danger"></div>
										<script type="text/javascript">
											$("#tenDangNhap").on('keyup',function(){
												$.ajax({
													type:'POST',
													url:'<%=request.getContextPath()+"/valid-username"%>',
													data:{username:$("#tenDangNhap").val()},
													success:function(data){
														if($('#tenDangNhap').val()==''){
															$("#message").text('Username not null');	
														}else{
															$("#message").text(data['message']);
														}
														if(data['message']=='OK'){
															$("#submit").removeAttr('disabled');
														}else{
															$('#submit').attr('disabled','disabled');
														}
													}
												});
											});
										</script>
									</div>
								</div>
								<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
									<div class="form-group">
										<label for="matKhau">Mật khẩu</label>
										<input type="password" class="form-control" id="matKhau" name="matKhau" placeholder="Mật khẩu">
										<s:fielderror class="text-danger" fieldName="password"></s:fielderror>
									</div>
								</div>
								<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
									<div class="form-group">
										<label for="ngayNoptien">Nhóm</label>
										<select name="nhomIds" id="nhom" class="form-control" multiple="multiple">
											<s:iterator var="nhom" value="nhoms">
												<option value="${nhom.id}"><s:property value="%{#nhom.ten}"/></option>
											</s:iterator>
										</select>
									</div>
								</div>
								<div class="col-xs-12">
									<button type="submit" id="submit" class="btn btn-sm btn-primary pull-right">Submit</button>
								</div>
							</form>
						</div>
					</div>
				</div>
				
				<div role="tabpanel" class="tab-pane ${active==true?'active':''} border-secondary" id="search">
					<div class="container-fluid">
						<div class="row"><span class="close btn btn-sm" onclick="closeModal()">&times;</span></div>
						<div class="row">
							<form action="<%=request.getContextPath()+"/search"%>" method="POST">
								<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
									<div class="form-group">
										<label for="taikhoan">Tài khoản</label>
										<s:textfield type="text" class="form-control" id="taikhoan" name="taikhoan" placeholder="Tài khoản"></s:textfield>
									</div>
								</div>
								<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
									<div class="form-group">
										<label for="sdt">Số điện thoại</label>
										<s:textfield type="number" class="form-control" id="sdt" name="sdt" placeholder="Số điện thoại"></s:textfield>
									</div>
								</div>
								<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
									<div class="form-group">
										<label for="ngaytao">Ngày tạo</label>
										<s:textfield type="date" class="form-control" id="ngaytao" name="ngaytao"></s:textfield>
									</div>
								</div>
								<div class="col-xs-12">
									<s:a action="all" class="btn btn-sm btn-default pull-right">Remove</s:a>
									<button type="submit" class="btn btn-sm btn-default pull-right">Search</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<script>
			function closeModal(){
				$(".tab-pane").removeClass('active');
				$(".choose").attr('aria-expanded','false');
				$(".choose").parent().removeClass('active');
			}
		</script>
	</div>
	
	<!-- Table -->
	<div class="container">
		<div class="table-responsive">
			<table class="table table-hover">
				<thead>
					<tr>
						<th>Tên đăng nhập</th>
						<th>Số điện thoại</th>
						<th>Ngày tạo</th>
						<th colspan="3" width="5%"></th>
					</tr>
				</thead>
				<tbody>
				<s:iterator var="nsd" value="nsds">
					<tr>
						<td>${nsd.tenDangNhap}</td>
						<td>${nsd.dienThoai }</td>
						<td>${nsd.ngayTao }</td>
						<td>
							<s:url var="urlU" value="/detail">
								<s:param name="id" value="%{#nsd.id}"></s:param>
							</s:url>
							<s:a href="%{#urlU}" class="btn btn-sm btn-default">Detail</s:a>
						</td>
						<td><button class="btn btn-sm btn-default" data-toggle="modal" href='#modal-id' onclick="del('${nsd.id}')">Delete</button></td>
					</tr>
				</s:iterator>
				</tbody>
			</table>
			Records: <select id="totalRec" class="btn btn-sm btn-default">
						<option ${total==2 ? 'selected':''} value="2">2</option>
						<option ${total==10 ? 'selected':''} value="10">10</option>
						<option ${total==15 ? 'selected':''} value="20">20</option>
						<option ${total==20 ? 'selected':''} value="30">30</option>
					</select><br>
					<script>
						$("#totalRec").on('change',function(){
							window.location.href = '<%=request.getContextPath()+"/nsd?total="%>'+$("#totalRec").val();
						});
					</script>
			<ul class="pagination">
				<s:url var="first" value="">
					<s:param name="page" value="1"></s:param>
				</s:url>
				<li><a href="<%=request.getContextPath()+"/nsd?page=1&total="%>${total}">&laquo;</a></li>
				<s:iterator value="pagination" var="p">
					<li class="${page==p ? 'active':''}"><a href="<%=request.getContextPath()+"/nsd?page="%>${p}<%="&total="%>${total}">${p}</a></li>
				</s:iterator>
				<li><a href="<%=request.getContextPath()+"/nsd?page="%>${totalPages}<%="&total="%>${total}">&raquo;</a></li>
			</ul>
		</div>
	</div>

	<div class="modal fade" id="modal-id">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">NOTIFICATION</h4>
				</div>
				<div class="modal-body">
					CONFIRM DELETE?
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<a id="cf" href="" type="button" class="btn btn-danger">Confirm</a>
				</div>
			</div>
		</div>
	</div>
	<script>
		function del(nsd){
			$("#cf").attr('href','<%=request.getContextPath()+"/nsd/delete?id="%>'+nsd);
		}
	</script>
</body>
</html>