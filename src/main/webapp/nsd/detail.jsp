<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Người sử dụng</title>
<!-- Latest compiled and minified CSS & JS -->
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"
	integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7"
	crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-2.2.4.js"
	integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI="
	crossorigin="anonymous"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"
	integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS"
	crossorigin="anonymous"></script>
<style>
.close:hover {
	color: red;
	opacity: 1;
}
</style>
</head>
<body>
	<nav class="navbar navbar-default" role="navigation">
	<div class="container-fluid">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse"
				data-target=".navbar-ex1-collapse">
				<span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="#">TTHC</a>
		</div>

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse navbar-ex1-collapse">
			<ul class="nav navbar-nav">
				<li><a href="#">Người sử dụng</a></li>
				<li><a href="#">Quản lý nhóm</a></li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li class="dropdown"><a href="#" class="dropdown-toggle"
					data-toggle="dropdown">User <b class="caret"></b></a>
					<ul class="dropdown-menu">
						<li><a href="#">Action</a></li>
						<li><a href="#">Another action</a></li>
						<li><a href="#">Something else here</a></li>
						<li><a href="#">Separated link</a></li>
					</ul></li>
			</ul>
		</div>
		<!-- /.navbar-collapse -->
	</div>
	</nav>
	<div class="container">
		
		<div class="row">
			<div class="col-lg-3 col-md-3 col-sm-3"></div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<form action="<%=request.getContextPath()+"/nsd/update" %>" method="post">
					<input name="id" type="hidden" value="${eitem.id}">
					<div class="form-group">
						<label class="label">Tên đăng nhập</label> <input type="text"
							name="tenDangNhap" class="form-control" value='${eitem.tenDangNhap}'>
					</div>

					<div class="form-group">
						<label class="label">Mật khẩu</label> 
						<input type="password" name="matKhau" class="form-control" value="${eitem.matKhau}">
					</div>

					<div class="form-group">
						<label class="label">Nhóm</label> 
						
						<select name="nhomIds" id="nhom" class="form-control" multiple>
							<s:iterator var="nhom" value="nhoms" step="">
								<s:set var="break" value="%{false}"></s:set>
								<s:iterator var="id" value="nhomIds">
									<s:if test="%{#id == #nhom.id}">
										<s:set var="break" value="%{true}"></s:set>
										<option selected="selected" value="${nhom.id}">
											<s:property value="%{#nhom.ten}" />
										</option>
									</s:if>
								</s:iterator>	
								<s:if test="!#break">
									<option value="${nhom.id}">
										<s:property value="%{#nhom.ten}" />
									</option>
								</s:if>
							</s:iterator>
						</select>
					</div>
					<span class="label label-success">${message }</span>
					<s:url var="back" value="/nsd"></s:url>
					<s:a href="%{#back}" class="btn btn-default pull-right">Back</s:a>
					<button type="submit" class="btn btn-default pull-right">Update</button>
				</form>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-3"></div>
		</div>
	</div>
</body>
</html>