<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script src="https://code.jquery.com/jquery-3.4.1.js"
	integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
	crossorigin="anonymous"></script>
</head>
<body>
	<script>
		var timer;

		function setTime() {
			timer = setTimeout(function() {
				alert('Session timeout');
				window.location.href='<%=request.getContextPath()+"/logout"%>';
			}, 3000);
		}

		function clearTime() {
			clearTimeout(timer);
		}

		var arr = [ "mouseover", "mouseout", "click", "keyup", "keydown",
				"keypress","change","mouseleave","mousemove","mousedown","mouseup","mouseenter"];

		for (var i = 0; i < arr.length; i++) {
			console.log(arr[i]);
			$('*').on(arr[i], function(event) {
				if (this === event.target) { 
					console.log('reset');
					clearTime();
					setTime();
				}
			});
		}
	</script>
</body>
</html>