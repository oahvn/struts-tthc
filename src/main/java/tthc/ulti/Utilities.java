package tthc.ulti;

import java.util.ArrayList;
import java.util.List;

public class Utilities {
	public static List<Integer> pagination(int page, long totalPages) {
		List<Integer> paginations = new ArrayList<Integer>();
		if (totalPages < 5) {
			for (int i = 1; i <= totalPages; i++) {
				paginations.add(i);
			}
		} else {
			if (page >= 3 && page < totalPages - 2) {
				for (int i = page - 2; i <= page + 2; i++) {
					paginations.add(i);
				}
			} else if (page < 3) {
				for (int i = 1; i <= 5; i++) {
					paginations.add(i);
				}
			} else {
				for (int i = (int) (totalPages - 4); i <= totalPages; i++) {
					paginations.add(i);
				}
			}
		}
		return paginations;
	}
}
