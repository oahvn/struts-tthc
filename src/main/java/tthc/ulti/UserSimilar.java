package tthc.ulti;

import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserSimilar {
	private String taikhoan;
	private String sdt;
	@Temporal(TemporalType.DATE)
	private Date ngaytao;
}
