package tthc.ulti;

public class Pageable {
	private int page;
	private int total;
	private int totalPages;
	private String sortName;
	private String sortType;

	public Pageable() {
		super();
	}

	public Pageable(int page, int total, int totalPages, String sortName, String sortType) {
		super();
		this.page = page;
		this.total = total;
		this.totalPages = totalPages;
		this.sortName = sortName;
		this.sortType = sortType;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public int getTotalPages() {
		return totalPages;
	}

	public void setTotalPages(int totalPages) {
		this.totalPages = totalPages;
	}

	public String getSortName() {
		return sortName;
	}

	public void setSortName(String sortName) {
		this.sortName = sortName;
	}

	public String getSortType() {
		return sortType;
	}

	public void setSortType(String sortType) {
		this.sortType = sortType;
	}

}
