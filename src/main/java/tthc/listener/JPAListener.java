package tthc.listener;

import javax.persistence.Persistence;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class JPAListener implements ServletContextListener {

	public static final String FACTORY = "FACTORY";

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		arg0.getServletContext().setAttribute(FACTORY,
				Persistence.createEntityManagerFactory("tthc", System.getProperties()));
	}

}
