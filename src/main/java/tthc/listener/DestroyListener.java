package tthc.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.struts2.ServletActionContext;

public class DestroyListener implements ServletContextListener {
	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		System.out.println("DESTROY Servlet context");
		ServletActionContext.getRequest().getSession(true).removeAttribute("User");
		ServletActionContext.getRequest().getSession(true).invalidate();
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		// TODO Auto-generated method stub

	}

}
