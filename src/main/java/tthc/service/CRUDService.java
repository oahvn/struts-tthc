package tthc.service;

import java.util.List;

public interface CRUDService {
	<T> void save(T object);

	<T> void update(T object);

	<T> void delete(T object, Object id);

	<T> T findById(Class<T> clazz, Object id);

	<T> List<T> findAll(Class<T> clazz);
}
