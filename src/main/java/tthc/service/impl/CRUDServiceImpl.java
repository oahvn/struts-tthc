package tthc.service.impl;

import java.util.List;

import tthc.repository.CRUDRepository;
import tthc.repository.impl.CRUDRepositoryImpl;
import tthc.service.CRUDService;

public class CRUDServiceImpl implements CRUDService {

	private CRUDRepository repo;

	public CRUDServiceImpl() {
		repo = new CRUDRepositoryImpl();
	}

	@Override
	public <T> void save(T object) {
		repo.save(object);
	}

	@Override
	public <T> void update(T object) {
		repo.update(object);
	}

	@Override
	public <T> void delete(T object, Object id) {
		repo.delete(object, id);
	}

	@Override
	public <T> T findById(Class<T> clazz, Object id) {
		return repo.findById(clazz, id);
	}

	@Override
	public <T> List<T> findAll(Class<T> clazz) {
		return repo.findAll(clazz);
	}

}
