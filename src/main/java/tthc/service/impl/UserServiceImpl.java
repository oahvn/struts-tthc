package tthc.service.impl;

import java.util.List;

import tthc.entity.HtNsd;
import tthc.repository.UserRepository;
import tthc.repository.impl.UserRepositoryImpl;
import tthc.service.UserSerivce;
import tthc.ulti.Pageable;
import tthc.ulti.UserSimilar;

public class UserServiceImpl implements UserSerivce {

	private UserRepository repo;

	public UserServiceImpl() {
		this.repo = new UserRepositoryImpl();
	}

	@Override
	public void save(HtNsd nsd) {
		repo.save(nsd);
	}

	@Override
	public HtNsd findById(String id) {
		return repo.findById(id);
	}

	@Override
	public HtNsd findByTenDangNhapAndMatKhau(HtNsd user) {
		return repo.findByTenDangNhapAndMatKhau(user.getTenDangNhap(), user.getMatKhau());
	}

	@Override
	public boolean exists(String username) {
		return repo.exists(username);
	}

	@Override
	public void update(HtNsd nsd) {
		repo.update(nsd);
	}

	@Override
	public List<HtNsd> findByConditions(UserSimilar similar, Pageable pageable) {
		return repo.findByConditions(similar, pageable);
	}

	@Override
	public void delete(HtNsd nsd) {
		repo.delete(nsd);
	}

	@Override
	public long count(UserSimilar similar) {
		return repo.count(similar);
	}

}
