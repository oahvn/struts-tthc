package tthc.service;

import java.util.List;

import tthc.entity.HtNsd;
import tthc.ulti.Pageable;
import tthc.ulti.UserSimilar;

public interface UserSerivce {
	public void save(HtNsd nsd);

	public void update(HtNsd nsd);

	public void delete(HtNsd nsd);

	public HtNsd findById(String id);

	public HtNsd findByTenDangNhapAndMatKhau(HtNsd user);

	public boolean exists(String username);

	public List<HtNsd> findByConditions(UserSimilar similar, Pageable pageable);

	public long count(UserSimilar similar);
}
