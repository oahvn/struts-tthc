package tthc.interceptor;

import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.Interceptor;

public class CustomInterceptor implements Interceptor {

	private static final long serialVersionUID = 1L;

	@Override
	public void destroy() {
		System.out.println("DESTROY INTERCEPTOR");
	}

	@Override
	public void init() {
		System.out.println("INIT INTERCEPTOR");
	}

	@Override
	public String intercept(ActionInvocation actionInvk) throws Exception {
		String result = actionInvk.invoke();
		System.out.println("Interceptor: " + result);
		return result;
	}

}
