package tthc.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import tthc.entity.HtNhomNsd;
import tthc.entity.HtNhomQuyen;
import tthc.entity.HtNsd;

public class AuthenticationFilter implements Filter {

	private List<String> guest;

	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

	@Override
	public void doFilter(ServletRequest arg0, ServletResponse arg1, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest) arg0;
		HttpServletResponse response = (HttpServletResponse) arg1;

		String uri = request.getRequestURI();
		int index = uri.lastIndexOf("/");
		String resources = uri.substring(index + 1);

		HttpSession session = request.getSession();

		if (session.getAttribute("User") == null) {
			if (guest.contains(resources)) {
				chain.doFilter(request, response);
			} else {
				response.sendRedirect(request.getContextPath() + "/login");
			}
		} else {
			HtNsd nsd = (HtNsd) session.getAttribute("User");
			List<String> roles = new ArrayList<>();

			for (HtNhomNsd nhomnsd : nsd.getHtNhomNsdsForHtNsdId()) {
				for (HtNhomQuyen nhomquyen : nhomnsd.getHtNhom().getHtNhomQuyens()) {
					roles.add(nhomquyen.getHtQuyen().getTenVietTat());
				}
			}

			if (resources.contains("save")) {
				if (roles.contains("ROLE_ADD_USER")) {
					chain.doFilter(request, response);
				} else {
					response.sendRedirect(request.getContextPath() + "/error");
				}
			} else if (resources.contains("delete")) {
				if (roles.contains("ROLE_DEL_USER")) {
					chain.doFilter(request, response);
				} else {
					response.sendRedirect(request.getContextPath() + "/error");
				}
			} else if (resources.contains("update")) {
				if (roles.contains("ROLE_EDIT_USER")) {
					chain.doFilter(request, response);
				} else {
					response.sendRedirect(request.getContextPath() + "/error");
				}
			} else {
				chain.doFilter(request, response);
			}

		}

	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		guest = new ArrayList<String>();
		guest.add("login");
		guest.add("login.action");
		guest.add("login-action");
		guest.add("login-action.action");
	}

}
