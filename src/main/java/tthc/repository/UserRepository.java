package tthc.repository;

import java.util.List;

import tthc.entity.HtNsd;
import tthc.ulti.Pageable;
import tthc.ulti.UserSimilar;

public interface UserRepository {
	public void save(HtNsd nsd);

	public void update(HtNsd nsd);

	public void delete(HtNsd nsd);

	public HtNsd findById(String id);

	public HtNsd findByTenDangNhapAndMatKhau(String tenDangNhap, String matKhau);

	public List<HtNsd> findAll();

	public List<HtNsd> findByConditions(UserSimilar similar, Pageable pageable);

	public long count(UserSimilar similar);

	public boolean exists(String username);
}
