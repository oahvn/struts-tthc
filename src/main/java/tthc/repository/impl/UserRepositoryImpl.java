package tthc.repository.impl;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

import tthc.entity.HtNsd;
import tthc.repository.UserRepository;
import tthc.ulti.JpaUtil;
import tthc.ulti.Pageable;
import tthc.ulti.UserSimilar;

public class UserRepositoryImpl implements UserRepository {

	private static EntityManager em = JpaUtil.getEntityManagerFactory().createEntityManager();

	public UserRepositoryImpl() {

	}

	@Override
	public void save(HtNsd nsd) {
		EntityManager em = JpaUtil.getEntityManagerFactory().createEntityManager();
		EntityTransaction et = null;
		try {
			et = em.getTransaction();
			et.begin();
			em.persist(nsd);
			et.commit();
			em.close();
		} catch (Exception e) {
			if (et.isActive()) {
				et.rollback();
			}
		}
	}

	@Override
	public void update(HtNsd nsd) {
		EntityManager em = JpaUtil.getEntityManagerFactory().createEntityManager();
		EntityTransaction et = null;
		try {
			et = em.getTransaction();
			et.begin();
			em.merge(nsd);
			et.commit();
			em.close();
		} catch (Exception e) {
			if (et.isActive()) {
				et.rollback();
			}
		}
	}

	@Override
	public void delete(HtNsd nsd) {
		EntityManager em = JpaUtil.getEntityManagerFactory().createEntityManager();
		EntityTransaction et = null;
		try {
			et = em.getTransaction();
			nsd = em.find(HtNsd.class, nsd.getId());
			if (nsd != null) {
				et.begin();
				em.remove(nsd);
				et.commit();
			}
			em.close();
		} catch (Exception e) {
			if (et.isActive()) {
				et.rollback();
			}
		}
	}

	@Override
	public HtNsd findById(String id) {
		EntityManager em = JpaUtil.getEntityManagerFactory().createEntityManager();
		HtNsd nsd = em.find(HtNsd.class, id);
		return nsd;
	}

	@Override
	public HtNsd findByTenDangNhapAndMatKhau(String tenDangNhap, String matKhau) {
		EntityManager em = JpaUtil.getEntityManagerFactory().createEntityManager();
		String jpql = "SELECT nsd FROM HtNsd nsd WHERE nsd.tenDangNhap = :tenDangNhap AND nsd.matKhau = :matKhau";
		Query query = em.createQuery(jpql);
		query.setParameter("tenDangNhap", tenDangNhap);
		query.setParameter("matKhau", matKhau);
		if (query.getResultList().isEmpty()) {
			return null;
		}
		HtNsd nsd = (HtNsd) query.getResultList().get(0);
		return nsd;
	}

	@Override
	public List<HtNsd> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<HtNsd> findByConditions(UserSimilar similar, Pageable pageable) {
		String jpql = "SELECT nsd FROM HtNsd nsd";
		String find = "";
		Map<String, Object> map = new HashMap<String, Object>();
		if (similar != null) {
			if (similar.getTaikhoan() != null && !similar.getTaikhoan().equals("")) {
				find += " nsd.tenDangNhap = :taikhoan AND ";
				map.put("taikhoan", similar.getTaikhoan());
			}
			if (similar.getSdt() != null && !similar.getSdt().equals("")) {
				find += " nsd.dienThoai = :sdt AND ";
				map.put("sdt", similar.getSdt());
			}
			if (similar.getNgaytao() != null && !similar.getNgaytao().toString().equals("")) {
				find += " to_date(nsd.ngayTao,'dd-MON-yy') = to_date(:ngaytao,'dd-MON-yy') AND";
				SimpleDateFormat format = new SimpleDateFormat("dd-MMM-yy");
				String dateString = format.format(similar.getNgaytao());
				map.put("ngaytao", dateString);
			}
			if (find.length() > 4) {
				jpql += " WHERE (" + find.substring(0, find.length() - 4) + ")";
			}
		}

		Query query = em.createQuery(jpql);

		if (similar != null && !map.isEmpty()) {
			map.forEach((key, value) -> {
				query.setParameter(key, value);
			});
		}

		if (pageable != null) {
			query.setFirstResult((pageable.getPage() - 1) * pageable.getTotal());
			query.setMaxResults(pageable.getTotal());
		}
		List<HtNsd> list = query.getResultList();
		return list;
	}

	@Override
	public long count(UserSimilar similar) {
		String jpql = "SELECT COUNT(*) FROM HtNsd nsd";
		String find = "";
		Map<String, Object> map = new HashMap<String, Object>();
		if (similar != null) {
			if (similar.getTaikhoan() != null && !similar.getTaikhoan().equals("")) {
				find += " nsd.tenDangNhap = :taikhoan AND ";
				map.put("taikhoan", similar.getTaikhoan());
			}
			if (similar.getSdt() != null && !similar.getSdt().equals("")) {
				find += " nsd.dienThoai = :sdt AND ";
				map.put("sdt", similar.getSdt());
			}
			if (similar.getNgaytao() != null && !similar.getNgaytao().toString().equals("")) {
				find += " to_date(nsd.ngayTao,'dd-MON-yy') = to_date(:ngaytao,'dd-MON-yy') AND";
				SimpleDateFormat format = new SimpleDateFormat("dd-MMM-yy");
				String dateString = format.format(similar.getNgaytao());
				map.put("ngaytao", dateString);
			}
			if (find.length() > 4) {
				jpql += " WHERE (" + find.substring(0, find.length() - 4) + ")";
			}
		}

		Query query = em.createQuery(jpql);

		if (similar != null && !map.isEmpty()) {
			map.forEach((key, value) -> {
				query.setParameter(key, value);
			});
		}

		long result = (long) query.getResultList().get(0);
		return result;
	}

	@Override
	public boolean exists(String username) {
		String jpql = "SELECT COUNT(*) FROM HtNsd nsd WHERE nsd.tenDangNhap = :username";
		Query query = em.createQuery(jpql);
		query.setParameter("username", username);
		long result = (long) query.getResultList().get(0);
		if (result > 0) {
			return true;
		}
		return false;
	}

}
