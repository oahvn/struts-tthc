package tthc.repository.impl;

import java.util.List;

import javax.persistence.EntityManager;

import tthc.repository.CRUDRepository;
import tthc.ulti.JpaUtil;
import tthc.ulti.Pageable;

public class CRUDRepositoryImpl implements CRUDRepository {
	private static EntityManager em = JpaUtil.getEntityManagerFactory().createEntityManager();

	public CRUDRepositoryImpl() {
	}

	@Override
	public <T> void save(T object) {
		EntityManager em = JpaUtil.getEntityManagerFactory().createEntityManager();
		try {
			em.getTransaction().begin();
			em.persist(object);
			em.getTransaction().commit();
			em.close();
		} catch (Exception e) {
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
			e.printStackTrace();
		}
	}

	@Override
	public <T> void update(T object) {
		EntityManager em = JpaUtil.getEntityManagerFactory().createEntityManager();
		try {
			em.getTransaction().begin();
			em.merge(object);
			em.getTransaction().commit();
			em.close();
		} catch (Exception e) {
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> void delete(T object, Object id) {
		EntityManager em = JpaUtil.getEntityManagerFactory().createEntityManager();
		try {
			object = (T) em.find(object.getClass(), id);
			if (object != null) {
				em.getTransaction().begin();
				em.remove(object);
				em.getTransaction().commit();
			}
			em.close();
		} catch (Exception e) {
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
		}
	}

	@Override
	public <T> T findById(Class<T> clazz, Object id) {
		EntityManager em = JpaUtil.getEntityManagerFactory().createEntityManager();
		T t = em.find(clazz, id);
		return t;
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> List<T> findAll(Class<T> clazz) {
		List<T> list = null;
		String jpql = "SELECT t FROM " + clazz.getSimpleName() + " t";
		list = em.createQuery(jpql).getResultList();
		return list;
	}

	@Override
	public <T> List<T> findByConditions(T similar, Pageable pageable) {
		// TODO Auto-generated method stub
		return null;
	}

}
