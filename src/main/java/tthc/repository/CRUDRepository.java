package tthc.repository;

import java.util.List;

import tthc.ulti.Pageable;

public interface CRUDRepository {
	<T> void save(T object);

	<T> void update(T object);

	<T> void delete(T object, Object id);

	<T> T findById(Class<T> clazz, Object id);

	<T> List<T> findAll(Class<T> clazz);

	<T> List<T> findByConditions(T similar, Pageable pageable);
}
