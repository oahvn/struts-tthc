package tthc.action.nsd;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.UUID;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

import lombok.Getter;
import lombok.Setter;
import tthc.entity.HtNhom;
import tthc.entity.HtNhomNsd;
import tthc.entity.HtNsd;
import tthc.service.CRUDService;
import tthc.service.UserSerivce;
import tthc.service.impl.CRUDServiceImpl;
import tthc.service.impl.UserServiceImpl;
import tthc.ulti.Pageable;
import tthc.ulti.UserSimilar;
import tthc.ulti.Utilities;

@Getter
@Setter
@Result(name = "input", location = "/nsd/index.jsp")
@ParentPackage("default")
@InterceptorRef(value = "customStack")
public class NSDAction extends ActionSupport implements ModelDriven<UserSimilar> {
	private static final long serialVersionUID = 1L;

	private static CRUDService crudService = new CRUDServiceImpl();
	private static UserSerivce userSerivce = new UserServiceImpl();

	private List<String> nhomIds = new ArrayList<String>();
	private List<HtNhom> nhoms = new ArrayList<>();
	private List<HtNsd> nsds = new ArrayList<>();
	private HtNsd eitem = new HtNsd();

	private String id;
	private String tenDangNhap;
	private String matKhau;

	private UserSimilar similar = new UserSimilar();

	private Integer page = 1;
	private Integer total = 2;
	private Long totalPages;
	private List<Integer> pagination = new ArrayList<>();

	private boolean hasError = false;
	private boolean isActive = false;

	private String message;

	public NSDAction() {
	}

	public void loadNhom() {
		nhoms = crudService.findAll(HtNhom.class);
	}

	public void load() {
		Pageable pageable = new Pageable();
		total = (total > 30) ? 30 : total;
		similar = (UserSimilar) ServletActionContext.getRequest().getSession().getAttribute("similar");
		long totalRecords = userSerivce.count(similar);
		totalPages = ((totalRecords % total == 0) ? totalRecords / total : totalRecords / total + 1);
		pagination.clear();
		pagination = Utilities.pagination(page, totalPages);

		pageable.setPage(page);
		pageable.setTotal(total);
		loadNhom();
		nsds = userSerivce.findByConditions(similar, pageable);
	}

	@Action(value = "/nsd", results = { @Result(name = "viewNSD", type = "dispatcher", location = "/nsd/index.jsp") })
	public String viewNSD() {
		load();
		return "viewNSD";
	}

	@Action(value = "/search", results = { @Result(name = "search", type = "dispatcher", location = "/nsd/index.jsp") })
	public String search() {
		isActive = true;
		ServletActionContext.getRequest().getSession().setAttribute("similar", similar);
		load();
		return "search";
	}

	@Action(value = "/all", results = { @Result(name = "success", type = "redirect", location = "/nsd") })
	public String all() {
		isActive = false;
		ServletActionContext.getRequest().getSession().removeAttribute("similar");
		return SUCCESS;
	}

	@Action(value = "/logout", results = { @Result(name = "logout", type = "redirect", location = "/login") })
	public String logout() {
		ServletActionContext.getRequest().getSession().removeAttribute("User");
		return "logout";
	}

	@Action(value = "/detail", results = {
			@Result(name = "detail", type = "dispatcher", location = "/nsd/detail.jsp") })
	public String viewDetail() {
		eitem = userSerivce.findById(id);
		for (HtNhomNsd nhomnsd : eitem.getHtNhomNsdsForHtNsdId()) {
			nhomIds.add(nhomnsd.getHtNhom().getId());
		}
		loadNhom();
		return "detail";
	}

	@Action(value = "/nsd/update", results = {
			@Result(name = "update", type = "dispatcher", location = "/nsd/detail.jsp") })
	public String updateNsd() {
		eitem = userSerivce.findById(id);
		eitem.setTenDangNhap(tenDangNhap);
		eitem.setMatKhau(matKhau);
		eitem.getHtNhomNsdsForHtNsdId().clear();

		Set<HtNhomNsd> nhomNsds = new HashSet<>();
		if (!nhomIds.isEmpty()) {
			for (String id : this.nhomIds) {
				HtNhomNsd nhomNsd = new HtNhomNsd();
				nhomNsd.setId(UUID.randomUUID().toString());
				nhomNsd.setNgayTao(new Date());
				nhomNsd.setHtNsdByHtNsdId(eitem);
				HtNhom nhom = crudService.findById(HtNhom.class, id);
				nhomNsd.setHtNhom(nhom);
				nhomNsds.add(nhomNsd);
			}
			eitem.getHtNhomNsdsForHtNsdId().addAll(nhomNsds);
		}

		userSerivce.update(eitem);
		load();
		message = "Update successed";
		return "update";
	}

	@Action(value = "/nsd/save", results = { @Result(name = "save", type = "redirect", location = "/nsd") })
	public String nsdSave() {
		HtNsd user = new HtNsd();
		user.setId(UUID.randomUUID().toString());
		user.setTenDangNhap(tenDangNhap);
		user.setMatKhau(matKhau);
		user.setNgayTao(new Date());
		user.setDienThoai(new Random(999999 - 1000000).nextInt() + 100000 + "");

		Set<HtNhomNsd> nhomNsds = new HashSet<>();
		if (!nhomIds.isEmpty()) {
			for (String id : this.nhomIds) {
				HtNhomNsd nhomNsd = new HtNhomNsd();
				nhomNsd.setId(UUID.randomUUID().toString());
				nhomNsd.setNgayTao(new Date());
				nhomNsd.setHtNsdByHtNsdId(user);
				HtNhom nhom = crudService.findById(HtNhom.class, id);
				nhomNsd.setHtNhom(nhom);
				nhomNsds.add(nhomNsd);
			}
		}
		user.setHtNhomNsdsForHtNsdId(nhomNsds);
		crudService.update(user);
		hasError = false;
		return "save";
	}

	@Action(value = "/nsd/delete", results = { @Result(name = "delete", type = "redirect", location = "/nsd") })
	public String delete() {
		HtNsd user = new HtNsd();
		user.setId(id);
		userSerivce.delete(user);
		ServletActionContext.getRequest().getSession().setAttribute("message", "Deleted a record");
		return "delete";
	}

	@Action(value = "/nsd/page", results = {
			@Result(name = "success", type = "dispatcher", location = "/nsd/index.jsp") })
	public String setPage() {
		return SUCCESS;
	}

	@Action(value = "/nsd/total", results = {
			@Result(name = "total", type = "dispatcher", location = "/nsd/index.jsp") })
	public String setTotal() {
		return "total";
	}

	@Override
	public void validate() {
		if (id == null) {
			if (userSerivce.exists(tenDangNhap)) {
				addFieldError("username", "Existing username");
			}
		}
		if (tenDangNhap != null) {
			if (tenDangNhap.trim().matches("/[^a-z0-9A-z]/") || tenDangNhap.trim().length() < 6) {
				addFieldError("username", "Invalid username");
			}
		}
		if (matKhau != null) {
			if (matKhau.trim().equals("") || matKhau.trim().length() < 6) {
				addFieldError("password", "Invalid password");
			}
		}
		if (hasFieldErrors()) {
			load();
		}
		hasError = hasFieldErrors();
	}

	@Override
	public UserSimilar getModel() {
		// TODO Auto-generated method stub
		return similar;
	}
}
