package tthc.action;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;

import com.opensymphony.xwork2.ActionSupport;

public class ViewAction extends ActionSupport {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Action(value = "/hello", results = { @Result(name = "hello", location = "/results/hello.jsp") })
	public String viewHello() {
		return "hello";
	}

	@Action(value = "/error", results = { @Result(name = "error", location = "/pages/error.jsp") })
	public String viewSuccess() {
		return ERROR;
	}

	@Action(value = "not-found", results = { @Result(name = "NOT_FOUND", location = "/pages/page-not-found.jsp") })
	public String notFound() {
		return "NOT_FOUND";
	}

}
