package tthc.action;

import java.util.Map;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import tthc.entity.HtNsd;
import tthc.service.UserSerivce;
import tthc.service.impl.UserServiceImpl;

@Getter
@Setter
@AllArgsConstructor
@Results(value = { @Result(name = "input", location = "/views/login.jsp") })
public class LoginAction extends ActionSupport implements ModelDriven<HtNsd>, SessionAware {
	private static final long serialVersionUID = 1L;
	private UserSerivce service;
	private HtNsd user = new HtNsd();
	private Map<String, Object> session;

	public LoginAction() {
		service = new UserServiceImpl();

	}

	@Action(value = "login-action", //
			results = { //
					@Result(name = "success", type = "redirect", location = "nsd"),//
			})
	public String login() {
		HtNsd user = service.findByTenDangNhapAndMatKhau(this.user);
		if (user != null) {
			session.put("User", user);
			return SUCCESS;
		} else {
			addActionMessage("Invalid username or password!");
			return INPUT;
		}
	}

	@Action(value = "/login", results = { @Result(name = "login", location = "/views/login.jsp") })
	public String viewLogin() {
		return "login";
	}

	@Override
	public HtNsd getModel() {
		return user;
	}

	@Override
	public void setSession(Map<String, Object> session) {
		this.session = session;
	}
}
