package tthc.action;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.opensymphony.xwork2.ActionSupport;

import tthc.service.UserSerivce;
import tthc.service.impl.UserServiceImpl;

@Result(name = "success", type = "json")
@ParentPackage("json-default")
public class RestAction extends ActionSupport {
	private static final long serialVersionUID = 1L;
	private String username;
	private String message;

	private static final UserSerivce service = new UserServiceImpl();

	@Action("valid-username")
	public String validUsername() {
		if (service.exists(username)) {
			message = "Existing username";
		} else {
			message = "OK";
		}
		return SUCCESS;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
